// import { HttpClient } from '@angular/common/http';
import { StudentService } from './../shared/student.service';
import { Student, StudentDegree } from './../shared/student.model';
import { SearchComponent } from './../search/search.component';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { HitApiService } from '../shared/hit-api.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  currentStudent: Student;
  degrees: StudentDegree[];
  formSubmitted = 'not submitted';
  messageExists = false;

  constructor(private _hitApi: HitApiService, private _studentService: StudentService) { }

  ngOnInit() {
    // Initialize the current student, their degrees and digital message status.
    this.currentStudent = this._studentService.getStudentInfo();
    this.degrees = this.currentStudent.degrees;
    if (this.currentStudent.digitalMessage !== '') {
      this.messageExists = true;
    }
  }

  // Prepare and POST form data
  onSubmitRsvp(form: NgForm) {
    this.assignFormValues(form);
    this.structureStudentFormData(this.currentStudent);
    this.postStudent(this.structureStudentFormData(this.currentStudent));
  }

  // Update the currentStudent with data from form
  assignFormValues(form: NgForm) {
    if (form.value.firstName) {
      this.currentStudent.name = {firstName: form.value.firstName, lastName: form.value.lastName};
    }
    if (form.value.digitalMessage) {
      this.currentStudent.digitalMessage = form.value.digitalMessage;
      // console.log(form.value.digitalMessage);
    }
    if (form.value.accessibility) {
      this.currentStudent.accessibility = form.value.accessibility;
    }
    this.currentStudent.isReserved = true;
  }

  // Use this to save currentStudent information to the service
  saveStudent(student) {
    this._studentService.saveStudentInfo(student);
    // console.log(this._studentService.getStudentInfo());
  }

  // Structure data for the database
  structureStudentFormData(student: Student) {
    const formData = new URLSearchParams();
    formData.append('EmplId', student.id.toString());
    formData.append('LastName', student.name.lastName);
    formData.append('FirstName', student.name.firstName);
    formData.append('GradReserved', student.isReserved.toString());
    if (student.accessibility !== '') {
      formData.append('GradAccessibility', student.accessibility);
    } else {
      formData.append('GradAccessibility', null);
    }
    if (student.digitalMessage !== '') {
      formData.append('TxtMessage', student.digitalMessage);
      // console.log('formdata is' + (formData.get('TxtMessage')));

    } else {
      formData.append('TxtMessage', null);
    }
    return formData;
  }

  // POST data
  postStudent(formData) {
    this._hitApi.createRsvp(formData).subscribe(
      data => {
        console.log('post was ' + data);
        this.formSubmitted = 'submitted';
        return true;
      },
      err => {
        console.log(err);
        this.formSubmitted = 'submission error';
        // return Observable.throw(err);
      },
      () => {
        this.saveStudent(this.currentStudent);
      }
    );
  }

}
