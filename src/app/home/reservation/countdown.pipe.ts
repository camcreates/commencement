import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countdown',
  pure: false
})

export class CountdownPipe implements PipeTransform {
transform(text: string, args: number) {
  if (text !== undefined) {
    const maxLength = args || 0;
    const length = text.length;

    return (maxLength - length);
  } else {
    return args;
  }
}
}
