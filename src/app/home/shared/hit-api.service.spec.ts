import { TestBed, inject } from '@angular/core/testing';

import { HitApiService } from './hit-api.service';

describe('HitApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HitApiService]
    });
  });

  it('should be created', inject([HitApiService], (service: HitApiService) => {
    expect(service).toBeTruthy();
  }));
});
