import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class HitApiService {
  private baseUrl = 'https://fscjcomm.azurewebsites.net/api/Comm/';
  private checkEligibleUrl = 'CheckEligible/';
  private degreeProfileUrl = 'DegreeProfile/';
  private gradRsvpUrl = 'GradRsvp';
  private retrievedData;

  constructor(public http: HttpClient) {
   }

  // Used to generate URL for CheckEligible GET request.
  checkEligible(inputId) {
    return this.baseUrl + this.checkEligibleUrl + inputId.toString();
  }

  // Used to generate URL for DegreeProfile GET request.
  degreeProfile(inputId) {
    return this.baseUrl + this.degreeProfileUrl + inputId.toString();
  }

  // Used to generate URL for GradRSVP POST request.
  gradRsvp() {
    return this.baseUrl + this.gradRsvpUrl;
  }

  // Return data from CheckEligible and DegreeProfile requests together.
  getStudentData(inputId) {
    return Observable.forkJoin(
      this.http.get(this.checkEligible(inputId)),
      this.http.get(this.degreeProfile(inputId))
    );
  }

  // POST data from reservation form to GradRsvp
  createRsvp(studentFormData) {
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };
    return this.http.post(this.gradRsvp(), studentFormData.toString(), options);
  }

}
