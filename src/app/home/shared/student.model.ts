export class Student {
    constructor(
        public id: number = 0,
        public name: StudentName = {lastName: '', firstName: ''},
        public isReserved: boolean = false,
        public degrees: StudentDegree[] = [],
        public digitalMessage: string = '',
        public accessibility: string = '') {
    }
}

export class StudentName {
    constructor(public lastName: string, public firstName: string) {
    }
}

export class StudentDegree {
    constructor(public degreeLevel: string, public degreeTitle: string, public honors: string = 'Honors not applicable') {
    }
}
