import { Injectable } from '@angular/core';
import { Student } from './student.model';

@Injectable()
export class StudentService {
  private student: Student;
  constructor() { }

  // Return currently stored student info.
  getStudentInfo() {
    return this.student;
  }

  // Update currently stored student info.
  saveStudentInfo(studentInfo: Student) {
    this.student = studentInfo;
  }
}



