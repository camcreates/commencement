import { HitApiService } from './shared/hit-api.service';
import { StudentService } from './shared/student.service';
import { HomeComponent } from './home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { ReservationComponent } from './reservation/reservation.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CountdownPipe } from './reservation/countdown.pipe';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [HomeComponent,
    SearchComponent,
    ReservationComponent,
    CountdownPipe
  ],
  entryComponents: [ReservationComponent
  ],
  exports: [HomeComponent
  ],
  providers: [StudentService,
    HitApiService
  ]
})
export class HomeModule { }
