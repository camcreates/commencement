import { HitApiService } from './../shared/hit-api.service';
import { StudentService } from './../shared/student.service';
import { Student, StudentName, StudentDegree } from './../shared/student.model';
import { Component, OnInit, AfterViewInit, ComponentFactoryResolver, ViewContainerRef, OnChanges, SimpleChanges, SimpleChange
 } from '@angular/core';
import { ReservationComponent } from '../reservation/reservation.component';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnChanges {
  studentData: any;
  degreeData: any;
  inputId: number;
  validIdSubmitted = false;
  reservationStatus = false;
  defaultString = '';
  showIneligibleMessage = false;
  showSearchForm = true;
  reservationComponentLoaded = false;

  currentStudent = new Student();

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private viewContainerRef: ViewContainerRef,
    private _studentService: StudentService, private _hitApiService: HitApiService) { }

    ngOnChanges(changes: SimpleChanges) {
      // Monitors value of validIdSubmitted to help determine if a valid id number has been submitted or not.
      const validId: SimpleChange = changes.validIdSubmitted;
    }

    // Triggered on clicking the Submit button.
    onClickSubmit(inputId) {
      this.searchId(inputId);
    }

    // Get student data based on submtited student ID
    searchId(inputId) {
      this._hitApiService.getStudentData(inputId).subscribe(
        data => {
          this.studentData = data[0][0];
          this.degreeData = data[1]; },
        err => { console.log(err); },
        () => {
          // console.log(this.studentData);
          // console.log(this.degreeData);
          this.convertEligibleStudent(this.studentData);
          this.updateDegrees(this.degreeData);
          this.saveAndLoadStudent();

        }
      );
    }

  // Update currentStudent general information with retrieved data
  convertEligibleStudent(searchData) {
    if (searchData.EmplId != null) {
      this.currentStudent.id = searchData.EmplId;
    }
    if (searchData.LastName != null) {
      this.currentStudent.name.lastName = searchData.LastName;
    }
    if (searchData.FirstName != null) {
      this.currentStudent.name.firstName = searchData.FirstName;
    }
    if (searchData.GradReserved != null) {
      this.currentStudent.isReserved = searchData.GradReserved;
    }
    if (searchData.GradAccessibility != null && searchData.GradAccessibility !== 'null') {
      this.currentStudent.accessibility = searchData.GradAccessibility;
    }
    if (searchData.TxtMessage != null) {
      this.currentStudent.digitalMessage = searchData.TxtMessage;
    }
    // console.log(this.currentStudent);
    return this.currentStudent;
  }

  // Update currentStudent degree information with retrieved data
  updateDegrees(searchData) {
    searchData.forEach(element => {
      // console.log(element);
      const degree = new StudentDegree(element.DegreeLevel, element.DegreeTitle, element.Honors);
      if  (element.Honors === '') {
       degree.honors = 'Honors not applicable';
      }
      this.currentStudent.degrees.push(degree);
    });
    // console.log(this.currentStudent);
    return this.currentStudent;
  }

  // In the case of invalid info, display the ineligibility message, otherwise save student info to
  // the student service and load the reservation form
  saveAndLoadStudent() {
    if (this.currentStudent.name.lastName.length < 1 || this.currentStudent.name.lastName === null) {
      // console.log(this.currentStudent.name.lastName);
      // console.log('invalid student');
      this.showIneligibleMessage = true;
    } else {
      this.showIneligibleMessage = false;
      this.showSearchForm = false;
      this.validIdSubmitted = true;
      // console.log(this.validIdSubmitted);
      this._studentService.saveStudentInfo(this.currentStudent);
      this.loadReservationPage();
    }
  }

  // Create the ReserverationComponent to display the reservation form
  loadReservationPage() {
    if (this.validIdSubmitted === true && this.reservationComponentLoaded === false) {
      const factory = this.componentFactoryResolver.resolveComponentFactory(ReservationComponent);
      const ref = this.viewContainerRef.createComponent(factory);
      ref.changeDetectorRef.detectChanges();
      this.reservationComponentLoaded = true;
    }
  }

}
